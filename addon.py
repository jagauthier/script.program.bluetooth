import os
import xbmcaddon, xbmcgui, xbmc
from threading import Thread
import time
import dbus
import dbus.service
import subprocess

__addon__     = xbmcaddon.Addon()
__addonpath__ = __addon__.getAddonInfo('path').decode("utf-8")

BT_ICON = __addonpath__ + "/resources/skins/default/media/bt_icon.jpg"

#global used to tell the worker thread the status of the window
__windowopen__   = True

#capture a couple of actions to close the window
ACTION_PREVIOUS_MENU = 10
ACTION_BACK = 92

# Dbus 
SERVICE_NAME = "org.bluez"
AGENT_IFACE = SERVICE_NAME + '.Agent1'
ADAPTER_IFACE = SERVICE_NAME + ".Adapter1"
DEVICE_IFACE = SERVICE_NAME + ".Device1"
PLAYER_IFACE = SERVICE_NAME + '.MediaPlayer1'
TRANSPORT_IFACE = SERVICE_NAME + '.MediaTransport1'

#control
BACK_BUTTON  = 1101
TITLE_LABEL  = 1102
BT_BUTTON    = 1103
DS_BUTTON    = 1104
PR_BUTTON    = 1105
STATUS_LABEL = 1106
BT_DISC_BG   = 1107
BT_DISC      = 1108
PAIR_LIST    = 1109

title="Bluetooth Settings"

def log(logline):
    print "BLUEXML: " + logline
    
def set_kodi_prop(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)
    xbmcgui.Window(10000).setProperty(property, strvalue)

def get_kodi_prop(property):
    value=xbmcgui.Window(10000).getProperty(property)
    log("Loading: '" + property + "', '" + value + "'")
    return value


def get_btdevices():
    bluetooth.paired_list.reset();
    del bluetooth.mac_list[:]
    del bluetooth.bt_path_list[:]    
    
    paired_devices=int(get_kodi_prop("paired_devices"))
    i=1
    while i<=paired_devices:
        bluetooth.paired_list.addItem(get_kodi_prop("bt_device" + str(i)))
        bluetooth.mac_list.append(get_kodi_prop("bt_mac" + str(i)))
        bluetooth.bt_path_list.append(get_kodi_prop("bt_devpath" + str(i)))
		 ### set listicon
        if get_kodi_prop("bt_device" + str(i)) == get_kodi_prop("connected_device"): 
            bluetooth.paired_list.getListItem(i-1).setIconImage(BT_ICON)
        else:
            bluetooth.paired_list.getListItem(i-1).setIconImage("")
        i+=1


def updateWindow():
    
    # I put this here because sometimes the thread starts before the onInit function
    # is complete. When that happens, it throws an exception.
    # We can assume that once statusbar is not None, it's done enough
    while bluetooth.statusbar==None:
        time.sleep(.25)
    #this is the worker thread that updates the window information every w seconds
    #this strange looping exists because I didn't want to sleep the thread for very long
    #as time.sleep() keeps user input from being acted upon
    
    old_bt=0
    
    while __windowopen__ and (not xbmc.abortRequested):
        
        get_btdevices()
        
        bluetooth.pair_name=get_kodi_prop("connected_device")
        if bluetooth.pair_name!="":
            bluetooth.statusbar.setLabel("Status: Connected to " + bluetooth.pair_name)
            bluetooth.button_bt.setVisible(True)
            bluetooth.btimage.setVisible(True)
        else:
            bluetooth.statusbar.setLabel("Status: Not connected")
            bluetooth.button_bt.setVisible(False)
            bluetooth.btimage.setVisible(False)
            
        powered=int(get_kodi_prop('bt_power'))
        bt_avail=int(get_kodi_prop('bt_avail'))
        discoverable=int(get_kodi_prop('bt_discoverable'))
        pairable=int(get_kodi_prop('bt_pairable'))
        
        if bt_avail==1 and old_bt==0:
            bluetooth.button_onoff.setEnabled(True)
            bluetooth.button_visible.setEnabled(True)
            bluetooth.button_pairable.setEnabled(True)
            bluetooth.paired_list.setEnabled(True)
                
        old_bt=bt_avail
        if bt_avail==0:
            bluetooth.button_onoff.setEnabled(False)
            bluetooth.button_visible.setEnabled(False)
            bluetooth.button_pairable.setEnabled(False)
            bluetooth.paired_list.setEnabled(False)
            bluetooth.paired_list.reset();
            del bluetooth.mac_list[:]
            bluetooth.statusbar.setLabel("Status: No bluetooth devices.")
            continue
            
        if powered:
            bluetooth.button_onoff.setSelected(True)
            bluetooth.button_visible.setEnabled(True)
            bluetooth.button_pairable.setEnabled(True)
        else:
            bluetooth.button_onoff.setSelected(False)
            bluetooth.button_visible.setEnabled(False)
            bluetooth.button_pairable.setEnabled(False)
            
        if pairable:
            bluetooth.button_pairable.setSelected(True)
        else:                
            bluetooth.button_pairable.setSelected(False)
                
        if discoverable:
            bluetooth.button_visible.setSelected(True)
        else:
            bluetooth.button_visible.setSelected(False)
            
        # give us a break
        time.sleep(1)
                                                                                                                                                
class bluetooth(xbmcgui.WindowXMLDialog):
    
    button_back=None
    button_onoff=None
    title_label=None
    button_visible=None
    button_pairable=None

    btimage=None
    button_bt=None

    paired_list=None
    mac_list = []
    bt_path_list = []

    statusbar=None
    
    def __init__(self,strXMLname, strFallbackPath, strDefaultName, forceFallback):
        log("Initializing...")

    def onInit(self):

        bluetooth.button_back=self.getControl(BACK_BUTTON)
        bluetooth.title_label=self.getControl(TITLE_LABEL)
        bluetooth.title_label.setLabel(title)
        
        bluetooth.button_onoff=self.getControl(BT_BUTTON)
        bluetooth.button_onoff.setRadioDimension(270,0, width=100, height=100)
        
        bluetooth.button_visible=self.getControl(DS_BUTTON)
        bluetooth.button_visible.setRadioDimension(270,0, width=100, height=100)
        
        bluetooth.button_pairable=self.getControl(PR_BUTTON)
        bluetooth.button_pairable.setRadioDimension(270,0, width=100, height=100)
        
        bluetooth.btimage=self.getControl(BT_DISC_BG)
        bluetooth.btimage.setVisible(False)
        bluetooth.button_bt=self.getControl(BT_DISC)
        bluetooth.button_bt.setVisible(False)        
    
        bluetooth.button_onoff.setEnabled(False)
        bluetooth.button_visible.setEnabled(False)
        bluetooth.button_pairable.setEnabled(False)
        
        bluetooth.paired_list=self.getControl(PAIR_LIST)
        bluetooth.paired_list.setEnabled(False)

        # do dbus connectivity
        self.bus = dbus.SystemBus()
        self.manager = dbus.Interface(self.bus.get_object("org.bluez", "/"), "org.freedesktop.DBus.ObjectManager")
        objects = self.manager.GetManagedObjects()
        
        for path, interfaces in objects.iteritems():
            if ADAPTER_IFACE in interfaces:
                log("Found adapter: " + path)
                set_kodi_prop("bluetooth_adapter", path)
                self.adapter=self.bus.get_object("org.bluez", path)
                self.adapter_manager=dbus.Interface(self.adapter, 'org.freedesktop.DBus.Properties')

        if get_kodi_prop("bluetooth_adapter")=="":
            xbmcgui.Dialog().ok("Device missing","No bluetooth devices are available.")            
            log("ENDING.")            
            __windowopen__ = False
            self.close()
            
        # This is loaded last, because we use it in the main looping thread
        # to make sure this function has completed initialization
        bluetooth.statusbar=self.getControl(STATUS_LABEL)
        bluetooth.statusbar.setLabel("")

            
            
    def onAction(self, action):
        global __windowopen__
        
        if action == ACTION_PREVIOUS_MENU:
            log("ENDING.")            
            __windowopen__ = False
            self.close()

    def onClick(self, controlID):
        log("onClick: " + str(controlID))        
        if controlID == BACK_BUTTON:
            log("ENDING.")            
            __windowopen__ = False
            self.close()

            
        if controlID == DS_BUTTON:
            if bluetooth.button_visible.isSelected() :
                self.adapter_manager.Set(ADAPTER_IFACE, 'Discoverable', True)
            else:                    
                self.adapter_manager.Set(ADAPTER_IFACE, 'Discoverable', False)

        if controlID == BT_BUTTON:
            if bluetooth.button_onoff.isSelected() :
                self.adapter_manager.Set(ADAPTER_IFACE, 'Powered', True)
            else:                  
                self.adapter_manager.Set(ADAPTER_IFACE, 'Powered', False)
 
        if controlID == PR_BUTTON:
            log("Selected pairing")            
            if bluetooth.button_pairable.isSelected() :
                self.adapter_manager.Set(ADAPTER_IFACE, 'Pairable', True)
            else:
                self.adapter_manager.Set(ADAPTER_IFACE, 'Pairable', False)
                    
        if controlID == PAIR_LIST:
                # we subtract one here, because this all starts at 1, not zero.
                position=bluetooth.paired_list.getSelectedPosition()
#                log("Selected position: " + str(position))
#                log("bt_path: " + bluetooth.bt_path_list[position])
#                log("bt_mac: " + bluetooth.mac_list[position])
#                log("item: " + bluetooth.paired_list.getSelectedItem().getLabel())
                device_name=bluetooth.paired_list.getSelectedItem().getLabel()
                device=self.bus.get_object("org.bluez", bluetooth.bt_path_list[position])
                device_properties=device.GetAll(DEVICE_IFACE, dbus_interface="org.freedesktop.DBus.Properties")
                device_manager=dbus.Interface(device, DEVICE_IFACE)                
                if (int(device_properties["Connected"]))==0:
                    connect=xbmcgui.Dialog().yesno(device_name,"Please choose to connect or unpair the device.",yeslabel="Connect",nolabel="Unpair")
                    if connect:
                        if bluetooth.bt_path_list[position]:
                            log("Connecting to: " + bluetooth.bt_path_list[position])
                            try:
                                bluetooth.statusbar.setLabel("Status: Connecting to " +device_name)
                                device_manager.Connect()
                            except:
                                xbmcgui.Dialog().ok(device_name,"Connection failed.")
                                return
                        bluetooth.pair_name=device_name
                    else:
                        ### unpair code - can't figure out how to do this from dbus :(
                        bt_disc="sudo bt-device -r " + bluetooth.mac_list[bluetooth.paired_list.getSelectedPosition()]
                        subprocess.call(bt_disc, shell=True)
                        discoverable=int(get_kodi_prop('bt_discoverable'))
                        if discoverable:
                            self.adapter_manager.Set(ADAPTER_IFACE, 'Discoverable', False)
                            self.adapter_manager.Set(ADAPTER_IFACE, 'Discoverable', True)
                        else:
                            self.adapter_manager.Set(ADAPTER_IFACE, 'Discoverable', True)
                            self.adapter_manager.Set(ADAPTER_IFACE, 'Discoverable', False)                            

                else:
                    device_manager.Disconnect()
                    xbmcgui.Dialog().ok(device_name,"Device disconnected.")
                    
        # bluetooth cancel button
        if controlID == BT_DISC:
            connected_path=get_kodi_prop("connected_path")
            if connected_path!="":
                device=self.bus.get_object("org.bluez", connected_path)
                device_properties=device.GetAll(DEVICE_IFACE, dbus_interface="org.freedesktop.DBus.Properties")
                device_manager=dbus.Interface(device, DEVICE_IFACE)
                device_manager.Disconnect()
            
            
    def onFocus(self, controlID):
        pass
    
    def onControl(self, controlID):
        pass
 
btdialog = bluetooth("bluetooth.xml", __addonpath__, 'default', '16x10')
t1 = Thread( target=updateWindow)
t1.setDaemon( True )
t1.start()
    
btdialog.doModal()
del btdialog



    




